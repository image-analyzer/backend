from django.urls import path
from .views import ImageAnalyzeAPIView

urlpatterns = [
    path("analyze_image/", ImageAnalyzeAPIView.as_view())
]
