import os

from PIL import ExifTags, Image
import pillow_heif
from collections import Counter
import matplotlib.pyplot as plt
from PIL.TiffImagePlugin import IFDRational


def converter(filename):
    heif_file = pillow_heif.read_heif(f'{filename}.HEIC')
    image = Image.frombytes(
        heif_file.mode,
        heif_file.size,
        heif_file.data,
        "raw",
    )
    image.save(f"{filename}.jpg", format("JPEG"))
    return f'{filename}.jpg'


def extract_colors(image):
    colors = image.getdata()
    color_counts = Counter(colors)
    popular_colors = color_counts.most_common(5)

    similar_colors = []

    for color in popular_colors:
        similar_color = find_similar_color(color[0])
        similar_colors.append(similar_color)

    return popular_colors, similar_colors


def find_similar_color(color):
    similar_color = (color[0] + 10, color[1] - 5, color[2] + 5)
    return similar_color


def extract_metadata(image):
    metadata_raw = image.info
    metadata = dict()

    if exif := image._getexif():
        for k, v in exif.items():
            if k not in ExifTags.TAGS:
                continue

            if type(v) not in [bytes, IFDRational]:
                metadata[ExifTags.TAGS[k]] = str(v)
    else:
        for k, v in metadata_raw.items():
            if type(v) not in [bytes]:
                metadata[k] = str(v)

    brightness_histogram = image.histogram()[:256]
    r_histogram = image.histogram()[0:256]
    g_histogram = image.histogram()[256:512]
    b_histogram = image.histogram()[512:768]

    return metadata, brightness_histogram, r_histogram, g_histogram, b_histogram


def process_image(image_path):
    filename, file_extension = os.path.splitext(image_path)

    if file_extension.lower() == ".heic":
        image_path = converter(filename)

    image = Image.open(image_path)

    popular_colors, similar_colors = extract_colors(image)
    metadata, brightness_hist, r_hist, g_hist, b_hist = extract_metadata(image)

    preview_path = f'image_analyzer/media/preview_{filename}.png'
    histogram_path = f'image_analyzer/media/histogram_{filename}.png'

    thumbnail = image.copy()
    thumbnail.thumbnail((128, 128))
    thumbnail.save(preview_path)

    plt.style.use('dark_background')
    plt.figure()
    plt.plot(brightness_hist, color='w', label='Brightness')
    plt.plot(r_hist, color='r', label='Red')
    plt.plot(g_hist, color='g', label='Green')
    plt.plot(b_hist, color='b', label='Blue')
    plt.legend()
    plt.title('Histograms')
    plt.savefig(histogram_path)

    return {
        'popular_colors': popular_colors,
        'similar_colors': similar_colors,
        'metadata': metadata,
        'preview_path': f'media/preview_{filename}.png',
        'histogram_path': f'media/histogram_{filename}.png',
    }