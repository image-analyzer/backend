from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .utils import process_image


class ImageAnalyzeAPIView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        file_objs = request.FILES.getlist('image')

        resp = dict()

        for file in file_objs:
            destination = open(file.name, 'wb+')
            for chunk in file.chunks():
                destination.write(chunk)
            destination.close()

            file_path = file.name
            resp[file.name] = process_image(file_path)
        return Response(data=resp)